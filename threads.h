#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <thread>
using namespace std;

void writePrimesToFile(int begin, int end, ofstream& file);//writes prime numbers into a file.
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N);//calls writePrimesToFile with N threads that run on a defind range, than print how long did it take.
