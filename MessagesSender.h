#include <iostream>
#include <string>
#include <set>
#include <queue>
#include <fstream>
#include <time.h>
#include <thread>
#include <mutex>
#include <condition_variable>

using namespace std;

class MessagesSender
{
public:
	MessagesSender();//c'tor
	~MessagesSender();//d'tor
	
	void Menu();//the menu
	void readFromData();//reads data from the file "data.txt"
	void writeMessages();//writes the data from "data.txt" to "output.txt"

private:
	set<string> _users;// a set of all the users connected
	queue<string> _data;// the data that is copied from "data.txt"
	mutex mu1;//data
	mutex mu2;//users
    condition_variable cond;
};