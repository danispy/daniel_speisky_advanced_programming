#include "MessagesSender.h"

using namespace std;

void main()
{
	MessagesSender m;
	thread readMessageT(&MessagesSender::readFromData, &m);
	thread writeMessageT(&MessagesSender::writeMessages, &m);
	readMessageT.detach();
	writeMessageT.detach();
	m.Menu();
}