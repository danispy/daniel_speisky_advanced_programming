#include "threads.h"
#include <time.h>
#include <mutex>

using namespace std;

mutex mu;
/*
the function writes prime numbers into a file.
input:
begin - begining of the range.
end - end of range.
file - the file that the function is writing the prime numbers to.
*/
void writePrimesToFile(int begin, int end, ofstream& file)
{
	bool flag;//if flag is true the number is not prime.
	for (int i = begin; i <= end; i++)
	{
		flag = false;
		for (int j = 2; j <= sqrt(i); j++)
		{
			if (i % j == 0)
			{
				flag = true;
				break;
			}
		}
		if (!flag)
		{
			mu.lock();
			file << i << "\n";
			mu.unlock();
		}
	}
}
/*
the function calls writePrimesToFile with N threads that run on a defind range, than print how long did it take.
input:
begin - begining of the range.
end - end of range.
filePath - the name of the file the prime numbers are written to.
N - numeber of created threads.
*/
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	vector<thread> threads;
	ofstream file(filePath, ios::out);
	int num = (end - begin) / N;

	clock_t beginTime = clock();
	for (int i = begin; i <= end; i+=num)
	{
		threads.push_back(thread(writePrimesToFile, i, i + num, ref(file)));
	}
	for (int i = 0; i <= N; i++)
	{
		threads[i].join();
	}

	cout << begin << " to " << end << " took " << float(clock() - beginTime) / CLOCKS_PER_SEC << " seconds" << endl;
}