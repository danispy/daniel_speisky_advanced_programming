#include "MessagesSender.h";

MessagesSender::MessagesSender()
{
}

MessagesSender::~MessagesSender()
{
}
/*
the function is a menu that presents 4 options to the user:
-log in
-log out
-show the connected users
*/
void MessagesSender::Menu()
{
	int choice = 5;
	string userName;
	while (choice != 4)
	{
		cout << "Choose one of the options below:" << endl;
		cout << "1. Sign in" << endl;
		cout << "2. Sign out" << endl;
		cout << "3. Show connected users" << endl;
		cout << "4. exit" << endl;
		cin >> choice;
		lock_guard<mutex> lock2(mu2);
		switch (choice)
		{
			case 1://log a new user in
				cout << "Enter your username" << endl;
				cin >> userName;
				while(_users.find(userName) != _users.end())
				{
					cout << "Username is already taken!\nPlease enter a different name" << endl;
					cin >> userName;
				}
				_users.insert(userName);
				cout << endl;
				break;
			case 2:// log a user out
				if (_users.empty())
				{
					cout << "There are no _users connected" << endl;
				}
				else
				{
					cout << "Enter your username" << endl;
					cin >> userName;
					while (_users.find(userName) == _users.end())
					{
						cout << "Username doesn't exist!\nPlease enter a different name" << endl;
						cin >> userName;
					}
					_users.erase(userName);
					cout << "Done" << endl;
				}
				break;
			case 3://show the connected users
				for (set<string>::iterator setIt = _users.begin(); setIt != _users.end(); setIt++)
				{
					cout << *setIt << endl;
				}
				cout << endl;
				break;
		}
	}
	if (choice == 4)
		exit(0);
}
/*
The function reads data from "data.txt" to a queue.
*/
void MessagesSender::readFromData()
{
	string line;
	while (true)
	{
		this_thread::sleep_for(chrono::seconds(60));
		fstream dataFile("data.txt", ios::in);
		if (dataFile.is_open())
		{
			if (dataFile.eof())
				dataFile.close();
			else
			{
				while (getline(dataFile, line))
				{
					unique_lock<mutex> lock1(mu1);
					_data.push(line);
					lock1.unlock();
					cond.notify_one();
				}
				dataFile.close();
				dataFile.open("data.txt", ios::out | ios::trunc);
				dataFile.close();
			}
		}
	}
}
/*
THe function rights the data from a queue to "output.txt" so any connected user gets the data.
*/
void MessagesSender::writeMessages()
{
	string line;
	fstream outFile("output.txt", ios::out);
	if(outFile.is_open())
	{
		while (true)
		{
			if (!_data.empty())
			{
				unique_lock<mutex> lock1(mu1);
				cond.wait(lock1);
				while (!_data.empty())
				{
					lock_guard<mutex> lock2(mu2);
					for (set<string>::iterator setIt = _users.begin(); setIt != _users.end(); setIt++)
					{
						outFile << *setIt << ": " << _data.front() << endl;
					}
					_data.pop();
				}
				lock1.unlock();
			}
		}
	}
}